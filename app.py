from flask import Flask, request, jsonify
import logging
import logging.config
import os
from CSV_MANAGE_API import csv_manage_routes

app = Flask(__name__)
app.register_blueprint(csv_manage_routes.app_csv_mgt)


if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    print("Started as GUNICORN")
    pid = os.getpid()
else:
    print("------> Started as APP")
    if __name__ == "__main__":
        # app.run(debug=True)
        app.run(host='0.0.0.0', port=5002, debug=True)