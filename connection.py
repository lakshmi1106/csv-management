import os
import logging
import yaml
import pymongo

# Import custom packages
from global_variable import connection_string,database_name

class ClsMongoDBInit():
    """ Initialize MongoDB Database connection"""

    global connection_string, database_name
    def get_ew_db_client() -> None:
        """ Returns a db client """
        client = pymongo.MongoClient(connection_string)
        print(database_name)
        ew_db = client[database_name]
        return ew_db

    def get_cl_db_client(cl_db) -> None:
        """ Returns a db client """
        client = pymongo.MongoClient(connection_string)
        cl_db = client[cl_db]
        return cl_db