from flask import Flask,jsonify,Blueprint,request
from CSV_MANAGE_FUNC.csv_manage_func import ClsCSVMgt
from werkzeug.utils import secure_filename
import os
app_csv_mgt = Blueprint('app_csv_mgt', __name__,url_prefix='/api/csv')
UPLOAD_FOLDER = 'static/csvfiles'





class CSVMgt():
    
    def __init__(self):
        pass
    
    @app_csv_mgt.route('/', methods=['POST'])
    def home_page():
        data = request.json
        client_number = data['client_number']
        result =  ClsCSVMgt()
        data = result.fun_to_get_csv(client_number)
        return jsonify(data)

    @app_csv_mgt.route('/save', methods=['POST'])
    def save_csv_file():
        # data = request.json
        # client_number = data['client_number']
        client_number = "917909763772"
        file = request.files['file']
        filename = secure_filename(file.filename)
        file_path = os.path.join(UPLOAD_FOLDER, filename)
        file.save(file_path)
        result =  ClsCSVMgt()
        data = result.func_to_save_csv(client_number,file_path)
        return data
    
    
        

    @app_csv_mgt.route('/test',methods=['POST'])
    def test():
        return "hello"
