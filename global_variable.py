# from connection import Connection

# import pymongo

import os

from dotenv import load_dotenv

load_dotenv(verbose=True)




connection_string = os.getenv("MONGO_CONNECTION_STRING")
database_name = os.getenv("EW_DB")
graph_url = "https://graph.facebook.com/v16.0/"