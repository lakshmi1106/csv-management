from flask import jsonify, request 
from connection import ClsMongoDBInit
import os


class ClsCSVMgt():
    def __init__(self):
        self.ew_db = ClsMongoDBInit.get_ew_db_client()
        self.client_db_name = "ew15_920504665545714"
        self.client_db = ClsMongoDBInit.get_cl_db_client(self.client_db_name)


    def fun_to_get_csv(self,client_id):
        csv_get_data = self.client_db.csvdatamanagement.find_one({"client_number":client_id}, {"_id":0})
        return csv_get_data
    
    
    def func_to_save_csv(self,client_id,file):
        csv_save_data = self.client_db.csvdatamanagement.update_one({"client_number":client_id},{"$set": {"file":file}})    
        return jsonify({"message":"uploaded successfully"})
    

#csvdatamanagement - {"client_number":"123","file":"http://document.com"}